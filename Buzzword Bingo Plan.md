# Buzzword Bingo – iOS App

## Purpose
The purpose of Buzzword Bingo is to put an app in the App Store that can bring a smile to someone's face during the boring "synergistic" strategy meetings, and to get the Solomon Solution name in the iOS app world.

## Process
Buzzword Bingo is going to be built for iPhones of all sizes (not universal to scale to iPads at launch). It will be written in Swift, and the code stored in Solomon's Bitbucket team account. I would like to make the project open-source, but that is up for discussion.

## Output
At the end of the project the goal would be to have the app in the iOS App Store, free to download. If we wanted to, as well, I could write a blog post or two about the process of building the app using Swift. How I built it and why I made some of the decisions that I did. If part of our goal is "thought leadership", then putting these things into the world can only help to bolster that perception among potential clients.