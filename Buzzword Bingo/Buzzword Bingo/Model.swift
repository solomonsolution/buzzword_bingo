//
//  Model.swift
//  Buzzword Bingo
//
//  Created by Jared Sorge on 10/24/14.
//  Copyright (c) 2014 Solomon Solution. All rights reserved.
//

import Foundation

let BuzzwordKEY = "Buzzwords"
let numberOfTiles = 8

func savedBuzzwords() -> [String] {
    let savedStrings = NSUserDefaults.standardUserDefaults().arrayForKey(BuzzwordKEY)
    var stringArray = [String]()
    
    if let stringObjectArray = savedStrings as? [String] {
        for theString in stringObjectArray {
            stringArray.append(theString)
        }
    }
    
    return stringArray
}

func saveBuzzwordList(newBuzzwords: [String]) {
    NSUserDefaults.standardUserDefaults().setObject(newBuzzwords, forKey: BuzzwordKEY)
}

func registerStandardDefaults() {
    let standardBuzzwords = [BuzzwordKEY: ["Synergy", "Engage", "Brand", "Content", "Value Add", "Game Changer", "Deliverable", "Monetize"]]
    NSUserDefaults.standardUserDefaults().registerDefaults(standardBuzzwords)
}

func drawRandom(#fromList:[String], quantity: Int = 8) -> [String] {
    var drawnBuzzwords = [String]()
    
    while drawnBuzzwords.count < quantity {
        let randomIndex = Int(arc4random_uniform(UInt32(fromList.count)))
        let drawnWord = fromList[randomIndex]
        if !contains(drawnBuzzwords, drawnWord) {
            drawnBuzzwords.append(drawnWord)
        }
    }
    
    return drawnBuzzwords
}

class BuzzwordGame {
    let activeBuzzwords: [BuzzwordItem]
    
    init(quantity: Int) {
        let allBuzzwords = savedBuzzwords()
        var drawnBuzzwords = drawRandom(fromList: allBuzzwords, quantity: quantity)
        var buzzwordSet = [BuzzwordItem]()
        for theBuzzword in drawnBuzzwords {
            buzzwordSet.append(BuzzwordItem(buzzword: theBuzzword))
        }
        activeBuzzwords = buzzwordSet
    }
}

class BuzzwordItem {
    let buzzword: String
    var selected: Bool
    
    init (buzzword: String) {
        self.buzzword = buzzword
        selected = false
    }
}