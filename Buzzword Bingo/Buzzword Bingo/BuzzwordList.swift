//
//  BuzzwordList.swift
//  Buzzword Bingo
//
//  Created by Jared Sorge on 10/24/14.
//  Copyright (c) 2014 Solomon Solution. All rights reserved.
//

import UIKit

class BuzzwordListViewController: UITableViewController, UITextFieldDelegate {
    //MARK: IBOutlet
    @IBOutlet var addButton: UIBarButtonItem!
    
    //MARK: Properties
    var doneAction: (()->())?
    lazy var buzzwords = savedBuzzwords()
    var buzzwordTextField: UITextField?
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItems = [self.addButton, self.editButtonItem()]
    }
    
    //MARK: IBActions
    @IBAction func addButtonTapped(sender: AnyObject) {
        let alertController = UIAlertController(title: "Add Buzzword", message: "Enter the new buzzword", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in
            if let newBuzzword = self.buzzwordTextField?.text {
                self.buzzwords.append(newBuzzword)
                let newIndexPath = NSIndexPath(forRow: (self.buzzwords.count - 1), inSection: 0)
                self.tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                
                saveBuzzwordList(self.buzzwords)
            }
            self.buzzwordTextField = nil
        }
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) -> Void in }
        alertController.addAction(cancelAction)
        
        alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
            self.buzzwordTextField = textField
        }
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func closeButtonTapped(sender: AnyObject) {
        doneAction?()
    }
    
    //MARK: UITableViewDataSource
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buzzwords.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let CellID = "BuzzwordCell"
        var cell: BuzzwordCell = tableView.dequeueReusableCellWithIdentifier(CellID, forIndexPath: indexPath) as! BuzzwordCell
        
        cell.buzzwordLabel.text = buzzwords[indexPath.row]
        
        return cell
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        let deleteString = buzzwords[indexPath.row]
        let buzzwordIndex = find(buzzwords, deleteString)!
        buzzwords.removeAtIndex(buzzwordIndex)
        let buzzwordIndexPath = NSIndexPath(forRow: buzzwordIndex, inSection: 0)
        self.tableView.deleteRowsAtIndexPaths([buzzwordIndexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        
        saveBuzzwordList(self.buzzwords)
    }
    
    //MARK: UITableViewDelegate
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
}

class BuzzwordCell: UITableViewCell {
    @IBOutlet var buzzwordLabel: UILabel!
}