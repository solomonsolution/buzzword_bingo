//
//  ViewController.swift
//  Buzzword Bingo
//
//  Created by Jared Sorge on 10/24/14.
//  Copyright (c) 2014 Solomon Solution. All rights reserved.
//

import UIKit

let Segue_ShowBuzzwords = "seg_showBuzzwordList"
let Segue_ShowPopover = "seg_webPopover"

let solomonBlue = UIColor(red: 21/255, green: 157/255, blue: 235/255, alpha: 1)

class ViewController: UIViewController {
    //MARK: Properties
    lazy var activeGame = BuzzwordGame(quantity: 8)
    
    @IBOutlet weak var box1Label: UILabel!
    @IBOutlet weak var box2Label: UILabel!
    @IBOutlet weak var box3Label: UILabel!
    @IBOutlet weak var box4Label: UILabel!
    @IBOutlet weak var box6Label: UILabel!
    @IBOutlet weak var box7Label: UILabel!
    @IBOutlet weak var box8Label: UILabel!
    @IBOutlet weak var box9Label: UILabel!
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerStandardDefaults()
        resetBoardForNewGame()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segue_ShowBuzzwords {
            let destination = segue.destinationViewController as! BuzzwordListViewController
            destination.doneAction = {()->() in
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        } else if segue.identifier == Segue_ShowPopover {
            let destiation = segue.destinationViewController as! WebViewController
            destiation.modalPresentationStyle = .FullScreen
            destiation.closeAction = {()->() in
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
    
    //MARK: IBAction
    @IBAction func editBuzzwordsButtonTapped(sender: AnyObject) {
        self.performSegueWithIdentifier(Segue_ShowBuzzwords, sender: nil)
    }
    
    @IBAction func newGameButtonTapped(sender: AnyObject) {
        self.activeGame = BuzzwordGame(quantity: 8)
        resetBoardForNewGame()
    }
    
    @IBAction func solomonButtonTapped(sender: AnyObject) {
        performSegueWithIdentifier(Segue_ShowPopover, sender: nil)
    }
    
    @IBAction func firstBoxTapped(sender: AnyObject) {
        selectBuzzword(self.box1Label)
    }
    
    @IBAction func secondBoxTapped(sender: AnyObject) {
        selectBuzzword(self.box2Label)
    }
    
    @IBAction func thirdBoxTapped(sender: AnyObject) {
        selectBuzzword(self.box3Label)
    }
    
    @IBAction func fourthBoxTapped(sender: AnyObject) {
        selectBuzzword(self.box4Label)
    }
    
    @IBAction func sixthBoxTapped(sender: AnyObject) {
        selectBuzzword(self.box6Label)
    }
    
    @IBAction func seventhBoxTapped(sender: AnyObject) {
        selectBuzzword(self.box7Label)
    }
    
    @IBAction func eigthBoxTapped(sender: AnyObject) {
        selectBuzzword(self.box8Label)
    }
    
    @IBAction func ninthBoxTapped(sender: AnyObject) {
        selectBuzzword(self.box9Label)
    }
    
    //MARK: Private
    private func resetBoardForNewGame () {
        resetBoxLabel(box1Label, with: activeGame.activeBuzzwords[0].buzzword)
        resetBoxLabel(box2Label, with: activeGame.activeBuzzwords[1].buzzword)
        resetBoxLabel(box3Label, with: activeGame.activeBuzzwords[2].buzzword)
        resetBoxLabel(box4Label, with: activeGame.activeBuzzwords[3].buzzword)
        resetBoxLabel(box6Label, with: activeGame.activeBuzzwords[4].buzzword)
        resetBoxLabel(box7Label, with: activeGame.activeBuzzwords[5].buzzword)
        resetBoxLabel(box8Label, with: activeGame.activeBuzzwords[6].buzzword)
        resetBoxLabel(box9Label, with: activeGame.activeBuzzwords[7].buzzword)
    }
    
    private func highlightBox (box: UIView) {
        box.backgroundColor = solomonBlue
    }
    
    private func selectBuzzword (buzzwordLabel: UILabel) {
        if buzzwordLabel.textColor == UIColor.whiteColor() {
            buzzwordLabel.textColor = solomonBlue
            buzzwordLabel.superview!.backgroundColor = UIColor.clearColor()
        } else {
            buzzwordLabel.textColor = UIColor.whiteColor()
            buzzwordLabel.superview!.backgroundColor = solomonBlue
        }
    }
    
    private func resetBoxLabel(label: UILabel, with buzzword: String) {
        label.text = buzzword.uppercaseString
        label.textColor = solomonBlue
        label.superview!.backgroundColor = UIColor.clearColor()
    }
}

@IBDesignable class BingoBox: UIView {
    //MARK: Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderColor = solomonBlue.CGColor
        layer.borderWidth = 1.0
    }
}

class WebViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    //MARK: Properties
    var closeAction: (()->())?
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = NSURL(string: "http://gosolomon.com")
        let request = NSURLRequest(URL: url!)
        self.webView.loadRequest(request)
        
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.rightBarButtonItem = closeButton
        
        self.closeButton.tintColor = solomonBlue
        self.backButton.tintColor = solomonBlue
    }
    
    //MARK: IBActions
    @IBAction func closeButtonTapped() {
        closeAction?()
    }
    
    @IBAction func backButtonTapped() {
        webView.goBack()
    }
}

class ModalSegue: UIStoryboardSegue {
    private var navigationController: UINavigationController!
    
    override init(identifier: String!, source: UIViewController, destination: UIViewController) {
        super.init(identifier: identifier, source: source, destination: destination)
        
        navigationController = UINavigationController(rootViewController: destination)
        navigationController.modalPresentationStyle = destination.modalPresentationStyle
    }
    
    override func perform() {
        sourceViewController.presentViewController(navigationController, animated: true, completion: nil)
    }
}