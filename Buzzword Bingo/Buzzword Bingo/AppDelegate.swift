//
//  AppDelegate.swift
//  Buzzword Bingo
//
//  Created by Jared Sorge on 10/24/14.
//  Copyright (c) 2014 Solomon Solution. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        registerStandardDefaults()
        return true
    }
}

